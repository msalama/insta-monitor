package com.instabug.instamonitor.repository;

import java.util.Map;

/**
 * Created by Mohamed Salama on 5/31/2016.
 */
public interface InstaMonitorRepository {

    void saveStatus(String key, long value);

    void reset();

    long getStatus(String key);

    boolean contains(String key);

    Map<String, ?> getAllSavedStats();

}
