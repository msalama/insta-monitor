package com.instabug.instamonitor.repository;

import android.content.SharedPreferences;

import java.util.Map;

/**
 * Created by Mohamed Salama on 5/31/2016.
 */
public class InMemoryInstaMonitorRepository implements InstaMonitorRepository {

    private final SharedPreferences sharedPreferences;

    public InMemoryInstaMonitorRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void saveStatus(String key, long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    @Override
    public void reset() {
        sharedPreferences.edit().clear().apply();
    }

    @Override
    public long getStatus(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    @Override
    public boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    @Override
    public Map<String, ?> getAllSavedStats() {
        Map<String,?> keys = sharedPreferences.getAll();
        return keys;
    }
}
