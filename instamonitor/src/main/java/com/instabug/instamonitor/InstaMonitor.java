package com.instabug.instamonitor;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.instabug.instamonitor.model.Screen;
import com.instabug.instamonitor.repository.InMemoryInstaMonitorRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by Mohamed Salama.
 *
 * InstaMonitor helps you to track user screen period.
 *
 * You can use this class by init it in your application class creation.
 * and then it will track all of your activities automatically.
 *
 */

public class InstaMonitor {

    private static final String TAG = "InstaMonitor";
    
    private static final String KEY_MONITOR_APP = "keyMonitorApp";
    private static final String EXTRA_APP_START_TIME = "startAppTimeExtra";
    private static final String EXTRA_START_TIME = "startTimeExtra";
    private static final String EXTRA_IS_IGNORED_ACTIVITY = "isIgnoredActivityExtra";

    private static InstaMonitor instaMonitor;
    private InMemoryInstaMonitorRepository inMemoryInstaMonitorRepository;


    /**
     * Singleton lazy init.
     * @param application your application instance.
     * @param enableTrackAllActivities to enable track all application activities.
     */

    public static void init(Application application, boolean enableTrackAllActivities){
        if(instaMonitor == null) {
            instaMonitor = new InstaMonitor(application, enableTrackAllActivities);
        } else {
            throw new RuntimeException("Init called twice");
        }
    }

    /**
     * Singleton getter.
     * @return  InstaMonitor instance.
     */
    
    public static InstaMonitor instance(){
        if(instaMonitor == null) {
            throw new RuntimeException("You must call init");
        }
        return instaMonitor;
    }

    /**
     * InstaMonitor Constructor.
     */

    public InstaMonitor(Application application, boolean enableTrackAllActivities) {
        if (enableTrackAllActivities) {
            application.
                    registerActivityLifecycleCallbacks(
                            new ActivityLifecycleCallbacks(this)
                    );
        }
        inMemoryInstaMonitorRepository = new
                InMemoryInstaMonitorRepository(getDefaultSharedPreferences(application));
    }

    /**
     * Start Monitor Application life cycle.
     * @param rootActivity your application root activity.
     */
    public void startMonitorApp(Activity rootActivity) {
        long startTime = System.currentTimeMillis();
        Intent intent = rootActivity.getIntent();
        intent.putExtra(EXTRA_APP_START_TIME, startTime);
    }

    /**
     * Stop Monitor Application life cycle.
     * @param rootActivity your application root activity.
     */
    public void stopMonitorApp(Activity rootActivity) {
        long startTime = rootActivity.getIntent().getLongExtra(EXTRA_APP_START_TIME, 0);
        savePeriod(KEY_MONITOR_APP, getTimeSpent(startTime));
    }

    /**
     * Start Monitor Activity.
     * @param activity the activity that you would like to start monitor.
     */
    public void startMonitorActivity(Activity activity) {
        long startTime = System.currentTimeMillis();
        Intent intent = activity.getIntent();
        intent.putExtra(EXTRA_START_TIME, startTime);
        intent.putExtra(EXTRA_IS_IGNORED_ACTIVITY, false);
    }

    /**
     * Stop Monitor Activity.
     * @param activity the activity that you would like to stop monitor.
     */
    public void stopMonitorActivity(Activity activity) {
        long startTime = activity.getIntent().getLongExtra(EXTRA_START_TIME, 0);
        savePeriod(activity.getLocalClassName(), getTimeSpent(startTime));
    }

    /**
     * Ignore Activity from automatic monitor.
     * @param activity the activity that you would like to ignore from automatic monitor.
     */
    public void ignoreMonitorActivity(Activity activity) {
        Intent intent = activity.getIntent();
        intent.putExtra(EXTRA_IS_IGNORED_ACTIVITY, true);
    }

    /**
     * Start monitor fragment.
     * @param fragment the fragment that you would like to start monitor.
     */
    public void startMonitorFragment(Fragment fragment) {
        long startTime = System.currentTimeMillis();
        Intent intent = fragment.getActivity().getIntent();
        intent.putExtra(EXTRA_START_TIME + fragment.getClass().getName(), startTime);
    }

    /**
     * Stop Monitor Fragment life cycle.
     * @param fragment the Fragment that you would like to stop monitor.
     */
    public void stopMonitorFragment(Fragment fragment) {
        long startTime = fragment.getActivity().getIntent().getLongExtra(
                EXTRA_START_TIME + fragment.getClass().getName()
                , 0
        );

        savePeriod(fragment.getClass().getName(), getTimeSpent(startTime));
    }

    /**
     * reset all saved stats.
     */
    public void reset() {
        inMemoryInstaMonitorRepository.reset();
    }


    /**
     * get application period.
     */
    public long getApplicationPeriod() {
        return inMemoryInstaMonitorRepository.getStatus(KEY_MONITOR_APP);
    }

    /**
     * get all Screens Stats.
     * @return list of screen objects.
     */
    public List<Screen> getAllStats() {

       List<Screen> screens = new ArrayList<>();

       Map<String, ?> allEntries = inMemoryInstaMonitorRepository.getAllSavedStats();

        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {

            Screen screen = new Screen(entry.getKey(), (Long)entry.getValue());
            screens.add(screen);
        }

        return screens;
    }

    /**
     * get period of time of App, Activity or fragment.
     * @return period of time
     */
    private long getTimeSpent(long startTime) {
        return System.currentTimeMillis() - startTime;
    }

    /**
     * save period of time of App, Activity or fragment.
     */
    private void savePeriod(String name, long period) {
        Log.d(TAG, "savePeriod: "+name +" : "+ TimeUnit.MILLISECONDS.toSeconds(period));
        
        long lastPeriod = 0;
        if (inMemoryInstaMonitorRepository.contains(name)) {
            lastPeriod = inMemoryInstaMonitorRepository.getStatus(name);
        }

        long newPeriod = lastPeriod + period;
        
        inMemoryInstaMonitorRepository.saveStatus(
                name,
                newPeriod
        );
    }
    
    private class ActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks{

        private InstaMonitor instaMonitor;

        public ActivityLifecycleCallbacks(InstaMonitor instaMonitor) {
            this.instaMonitor = instaMonitor;
        }

        private boolean isIgnoredActivity(Activity activity) {
            return activity.getIntent().getBooleanExtra(
                            EXTRA_IS_IGNORED_ACTIVITY,
                            false
                            );
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

            if (activity.isTaskRoot()) {
                instaMonitor.startMonitorApp(activity);
            }
            
            if (!isIgnoredActivity(activity)) {
                instaMonitor.startMonitorActivity(activity);
            }
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

            if (!isIgnoredActivity(activity)) {
                instaMonitor.stopMonitorActivity(activity);
            }

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            if (activity.isTaskRoot()) {
                instaMonitor.stopMonitorApp(activity);
            }

        }
    }
}
