package com.instabug.instamonitor.model;

public class Screen {

    private long period;
    private String name;

    public Screen(String name, long period) {
        this.name = name;
        this.period = period;
    }

    public long getPeriod() {
        return period;
    }

    public String getName() {
        return name;
    }
}
