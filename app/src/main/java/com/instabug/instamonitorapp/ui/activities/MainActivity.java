package com.instabug.instamonitorapp.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.instabug.instamonitor.InstaMonitor;
import com.instabug.instamonitorapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
