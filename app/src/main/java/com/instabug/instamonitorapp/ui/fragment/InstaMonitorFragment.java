package com.instabug.instamonitorapp.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.instabug.instamonitor.InstaMonitor;
import com.instabug.instamonitorapp.R;
import com.instabug.instamonitorapp.ui.activities.IgnoredActivity;
import com.instabug.instamonitorapp.ui.activities.SavedStatsActivity;

/**
 * Created by Mohamed Salama on 5/31/2016.
 */
public class InstaMonitorFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_instamonitor, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button btnViewAllStats = (Button) view.findViewById(R.id.btnViewAllStats);
        btnViewAllStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SavedStatsActivity.class);
                startActivity(intent);
            }
        });

        Button btnReset = (Button) view.findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InstaMonitor.instance().reset();
            }
        });

        Button btnIgnoredActivity = (Button) view.findViewById(R.id.btnIgnoreActivity);
        btnIgnoredActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), IgnoredActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        InstaMonitor.instance().startMonitorFragment(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        InstaMonitor.instance().stopMonitorFragment(this);
    }
}
