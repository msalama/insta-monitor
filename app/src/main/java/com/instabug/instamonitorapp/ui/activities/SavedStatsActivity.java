package com.instabug.instamonitorapp.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import com.instabug.instamonitor.InstaMonitor;
import com.instabug.instamonitor.repository.InMemoryInstaMonitorRepository;
import com.instabug.instamonitorapp.R;
import com.instabug.instamonitorapp.adpater.InstaMonitorResultsAdapter;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

public class SavedStatsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_stats);

        InstaMonitorResultsAdapter instaMonitorResultsAdapter =
                new InstaMonitorResultsAdapter(
                        InstaMonitor.instance().getAllStats()
                );

        //init recycle view
        RecyclerView recyclerViewProducts =
                (RecyclerView) findViewById(R.id.results_recycleView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerViewProducts.setLayoutManager(layoutManager);
        recyclerViewProducts.setHasFixedSize(true);

        recyclerViewProducts.setAdapter(instaMonitorResultsAdapter);
    }


}
