package com.instabug.instamonitorapp.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.instabug.instamonitor.InstaMonitor;
import com.instabug.instamonitorapp.R;

/**
 * Created by Mohamed Salama on 6/1/2016.
 */
public class IgnoredActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ignore);

        InstaMonitor.instance().ignoreMonitorActivity(this);
    }
}
