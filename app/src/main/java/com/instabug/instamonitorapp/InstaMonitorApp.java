package com.instabug.instamonitorapp;

import android.app.Application;

import com.instabug.instamonitor.InstaMonitor;

public class InstaMonitorApp extends Application {

    private InstaMonitor instaMonitor;

    @Override
    public void onCreate() {
        super.onCreate();
        InstaMonitor.init(this, true);
    }

}
