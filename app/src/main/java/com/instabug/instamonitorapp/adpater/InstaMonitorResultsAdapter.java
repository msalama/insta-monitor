package com.instabug.instamonitorapp.adpater;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.instabug.instamonitor.model.Screen;
import com.instabug.instamonitorapp.R;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

public class InstaMonitorResultsAdapter extends
        RecyclerView.Adapter<InstaMonitorResultsAdapter.ItemViewHolder> {

    private static final String TAG = "DevicesAdapter";

    private List<Screen> screens;

    public InstaMonitorResultsAdapter(List<Screen> screens) {
        setList(screens);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View ItemView = inflater.inflate(R.layout.screen_item, parent, false);
        ItemViewHolder holder = new ItemViewHolder(ItemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder itemViewHolder, int position) {

        Screen screen = screens.get(position);

        itemViewHolder.txtScreenName.setText(screen.getName());

        itemViewHolder.txtScreenPeriod.
                setText(String.valueOf(
                                TimeUnit.MILLISECONDS.toSeconds(screen.getPeriod())
                        ).concat(" sec")
                );
    }

    public Screen getItem(int position) {
        return screens.get(position);
    }

    public void replaceData(List<Screen> screens) {
        setList(screens);
        notifyDataSetChanged();
    }


    private void setList(@NonNull List<Screen> screens) {
        this.screens = checkNotNull(screens);
    }

    @Override
    public int getItemCount() {
        return screens.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtScreenName;
        public TextView txtScreenPeriod;

        public ItemViewHolder(View itemView) {
            super(itemView);
            txtScreenName = (TextView) itemView.findViewById(R.id.txtScreenName);
            txtScreenPeriod = (TextView) itemView.findViewById(R.id.txtScreenPeriod);
        }

    }

}
